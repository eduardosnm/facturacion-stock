<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => "Fabian Bolognesi",
            'email' => "fabianhbolognesi@gmail.com",
            'email_verified_at' => now(),
            'password' => Hash::make("Fabian2021"), // password
            'remember_token' => Str::random(10),
            'is_admin' => true
        ]);

        \App\User::create([
            'name' => "Eduardo Bravo",
            'email' => "eduardosnm@msn.com",
            'email_verified_at' => now(),
            'password' => Hash::make("biblioteca123"), // password
            'remember_token' => Str::random(10),
            'is_admin' => true
        ]);
    }
}
