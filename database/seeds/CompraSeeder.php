<?php

use Illuminate\Database\Seeder;

class CompraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Compra::class)->times(10)->create()
            ->each(function ($compra) {
                $cantidad = random_int(1, 10);
                factory(\App\CompraProducto::class)->times($cantidad)->create([
                    "compra_id" => $compra->id
                ]);
            });
    }
}
