<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('nombre');
            $table->text('descripcion');
            $table->unsignedInteger('cantidad')->default(0);
            $table->float('precio',12,2)->default(0);
            $table->float('markup',12,2)->nullable()->default(0);
            $table->boolean('estado')->default(true);
            $table->unsignedBigInteger('marca_id');
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('marca_id')->references('id')->on('marcas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('categoria_id')->references('id')->on('marcas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
