<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\CompraProducto::class, function (Faker $faker) {
    $precioUnitario = $faker->randomFloat(2, 1000, 10000);
    $cantidad = random_int(1, 10);
    return [
        "cantidad" => $cantidad,
        "total" => $cantidad * $precioUnitario,
        "precio_unitario" => $precioUnitario,
        "producto_id" => \App\Producto::inRandomOrder()->first()->id,
    ];
});
