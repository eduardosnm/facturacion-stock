<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        "id" => $faker->unique()->randomNumber(5),
        "nombre" => $faker->sentence(3, true),
        "descripcion" => $faker->sentence(6, true),
        "markup" => $faker->numberBetween(5, 25),
        "marca_id" => \App\Marca::inRandomOrder()->first()->id,
        "categoria_id" => \App\Categoria::inRandomOrder()->first()->id,
    ];
});
