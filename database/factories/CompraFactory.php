<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Compra;
use Faker\Generator as Faker;

$factory->define(Compra::class, function (Faker $faker) {
    return [
        "total" => $faker->randomFloat(2, 1000, 100000),
        "user_id" => \App\User::first()->id
    ];
});
