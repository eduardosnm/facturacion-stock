<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
{{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="/css/app.css">
</head>
<body class="layout-navbar-fixed sidebar-mini">
<div id="app">
</div>

@auth
    <script>
        window.user = @json(auth()->user())
    </script>
@endauth
<script src="/js/app.js"></script>

</body>
</html>
