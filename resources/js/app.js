require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router';
import App from "./App";
import routes from './routes';
import store from './store';
import VueSweetalert2 from "vue-sweetalert2";
import moment from 'vue-moment';
import { Form, HasError, AlertError } from 'vform/src';

Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(moment);
window.form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

const router = new VueRouter({
    routes: routes,
    mode: 'history'
});

const app = new Vue({
    el: '#app',
    router,
    store,
    render: typeof user !== 'undefined' ? h => h(App) : null,
});

