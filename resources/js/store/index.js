import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import carrito from './modules/carrito';
import loading from './modules/loading';

export default new Vuex.Store({
    modules: {
        carrito,
        loading,
    }
})
