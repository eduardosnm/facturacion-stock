export const CARGANDO = (state) => {
    state.isLoading = true;
}

export const FIN_CARGANDO = (state) => {
    state.isLoading = false;
}

