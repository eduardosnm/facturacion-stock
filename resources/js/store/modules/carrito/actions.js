export const agregarArticuloAlCarrito = ({ commit }, { producto, cantidad }) => {
    commit('AGREGAR_AL_CARRITO', { producto, cantidad });
}

export const setArticulosCarrito = ({ commit }) => {
    commit('SET_CARRITO')
}

export const eliminarArticuloDelCarrito = ({commit}, articulo) => {
    commit('ELIMINAR_ARTICULO_DEL_CARRITO', articulo);
}

export const limpiarCarrito = ({commit}) => {
    commit('VACIAR_CARRITO');
}

export const ocultarCarrito = ({commit}) => {
    commit('OCULTAR_CARRITO')
}

export const carritoVisible = ({commit}) => {
    commit('VISIBILIDAD_CARRITO')
}

export const vaciarCarrito = ({commit}) => {
    commit('VACIAR_CARRITO');
}

export const setCantidad = ({commit}, {producto, cantidad}) => {
    commit('SET_CANTIDAD', { producto, cantidad })
}
