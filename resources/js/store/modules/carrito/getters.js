export const cantidadArticulosCarrito = (state) => {
    return state.carrito.length;
}

export const carritoPrecioTotal = (state) => {
    let total = 0;

    state.carrito.forEach(item => {
        total += item.producto.precio * item.cantidad;
    });

    return total;
}
