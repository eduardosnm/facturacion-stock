
export const AGREGAR_AL_CARRITO = (state, { producto, cantidad }) => {
    let articuloEnElCarrito = state.carrito.find(item => {
        return item.producto.id === producto.id;
    });

    if (articuloEnElCarrito){
        articuloEnElCarrito.cantidad += cantidad;
        return;
    }

    state.carrito.push({producto, cantidad});
    window.localStorage.setItem('carrito', JSON.stringify(state.carrito));
}

export const ELIMINAR_ARTICULO_DEL_CARRITO = (state, producto) => {
    state.carrito = state.carrito.filter(item => {
        return item.producto.id !== producto.id;
    });
    window.localStorage.setItem('carrito', JSON.stringify(state.carrito));
}

export const SET_CARRITO = (state) => {
    let items = window.localStorage.getItem('carrito')
        ? JSON.parse(window.localStorage.getItem('carrito'))
        : []
    state.carrito = items;
}

export const OCULTAR_CARRITO = (state) => {
    state.visible = false;
}

export const VISIBILIDAD_CARRITO = (state) => {
    state.visible = !state.visible;
}

export const VACIAR_CARRITO = (state) => {
    state.carrito = [];
    window.localStorage.removeItem('carrito');
}

export const SET_CANTIDAD = (state, { producto, cantidad }) => {
    let index = state.carrito.findIndex(item => {
        return item.producto.id === producto.id;
    });

    if (index > -1){
        state.carrito[index].cantidad = cantidad;
        window.localStorage.removeItem('carrito');
        window.localStorage.setItem('carrito', JSON.stringify(state.carrito));
    }
}
