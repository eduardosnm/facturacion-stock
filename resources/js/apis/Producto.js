import Api from "./Api";

const END_POINT = 'productos';

export default {
    all(pagina) {
        return Api.get(END_POINT+'?page='+pagina);
    },
}
