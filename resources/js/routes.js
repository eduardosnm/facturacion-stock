import PaginaProductos from "./pages/PaginaProductos";
import PaginaCompras from "./pages/PaginaCompras";
import PaginaNuevaCompra from "./pages/PaginaNuevaCompra";
import PaginaVentas from "./pages/PaginaVentas";
import PaginaNuevaVenta from "./pages/PaginaNuevaVenta";
import ResumenPedido from "./pages/ResumenPedido";
import PaginaDetalleVenta from "./pages/PaginaDetalleVenta";
import PaginaUsuarios from "./pages/PaginaUsuarios";
import PaginaCategorias from "./pages/PaginaCategorias";
import PaginaMarcas from "./pages/PaginaMarcas";
import PaginaVentasDelDia from "./pages/PaginaVentasDelDia";


export default [
    {
        path: '/productos',
        name:'productos',
        component: PaginaProductos,
    },
    {
        path: '/compras',
        name:'compras',
        component: PaginaCompras
    },
    {
        path: '/compras/nueva',
        name:'compra_nueva',
        component: PaginaNuevaCompra
    },
    {
        path: '/ventas',
        name:'ventas',
        component: PaginaVentas
    },
    {
        path: '/ventas/nueva',
        name:'venta_nueva',
        component: PaginaNuevaVenta
    },
    {
        path: '/ventas/del-dia',
        name:'venta_del_dia',
        component: PaginaVentasDelDia
    },
    {
        path: '/ventas/:id',
        name:'venta',
        component: PaginaDetalleVenta,
        props: true
    },
    {
        path: '/resumen-pedidos',
        name:'resumen',
        component: ResumenPedido
    },
    {
        path: '/usuarios',
        beforeEnter(to, from, next) {
            if (!parseInt(user.is_admin)) {
                next('productos');
            }
            next();
        },
        name:'usuarios',
        component: PaginaUsuarios
    },
    {
        path: '/categorias',
        name:'categorias',
        component: PaginaCategorias
    },
    {
        path: '/marcas',
        name:'marcas',
        component: PaginaMarcas
    },
    // { path: '/404', name:'not-found', component: NotFound },
    // { path: '*', component: NotFound }
]
