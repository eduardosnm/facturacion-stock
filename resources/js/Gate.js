export default class Gate {

    constructor(user) {
        this.user = user;
    }

    isAdmin(){
        return parseInt(this.user.is_admin);
    }

}
