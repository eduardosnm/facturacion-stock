<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';

    protected $guarded = [];

    public function productos()
    {
        return $this->hasMany(Producto::class, 'marca_id');
    }

    public function setNombreAttribute($value)
    {
        $this->attributes["nombre"] = ucfirst($value);
    }
}
