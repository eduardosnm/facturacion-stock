<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table = 'ventas';
    protected $guarded = [];

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'producto_venta',
            'venta_id',
            'producto_id')
            ->as('venta')
            ->withPivot("cantidad", "total", "precio_unitario","producto_id", "id");
    }

    public function productoVenta()
    {
        return $this->hasMany(ProductoVenta::class, 'venta_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function formaDePago()
    {
        return $this->belongsTo(FormaPago::class, 'forma_pago_id');
    }

    public function afip()
    {
        return $this->hasOne(Afip::class, 'venta_id');
    }
}
