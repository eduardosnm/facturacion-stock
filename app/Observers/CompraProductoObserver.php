<?php

namespace App\Observers;

use App\CompraProducto;

class CompraProductoObserver
{
    /**
     * Handle the compra producto "created" event.
     *
     * @param  \App\CompraProducto  $compraProducto
     * @return void
     */
    public function created(CompraProducto $compraProducto)
    {
        //
    }

    /**
     * Handle the compra producto "updated" event.
     *
     * @param  \App\CompraProducto  $compraProducto
     * @return void
     */
    public function updated(CompraProducto $compraProducto)
    {
        //
    }

    /**
     * Handle the compra producto "deleted" event.
     *
     * @param  \App\CompraProducto  $compraProducto
     * @return void
     */
    public function deleted(CompraProducto $compraProducto)
    {
        $compra = $compraProducto->compra;
        $compra->update([
            "total" => $compra->compraProducto->sum('total')
        ]);
    }

    /**
     * Handle the compra producto "restored" event.
     *
     * @param  \App\CompraProducto  $compraProducto
     * @return void
     */
    public function restored(CompraProducto $compraProducto)
    {
        //
    }

    /**
     * Handle the compra producto "force deleted" event.
     *
     * @param  \App\CompraProducto  $compraProducto
     * @return void
     */
    public function forceDeleted(CompraProducto $compraProducto)
    {
        //
    }
}
