<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Afip extends Model
{
    protected $table = 'afip';
    protected $dates = ['caeFchVto'];
    protected $guarded = [];

    public function venta()
    {
        return $this->belongsTo(Venta::class, 'venta_id');
    }
}
