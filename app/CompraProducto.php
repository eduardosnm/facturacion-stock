<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CompraProducto extends Pivot
{
    protected $table = 'compra_producto';
    protected $guarded = [];

    public function producto()
    {
        return $this->belongsTo(Producto::class, 'producto_id');
    }

    public function compra()
    {
        return $this->belongsTo(Compra::class, 'compra_id');
    }
}
