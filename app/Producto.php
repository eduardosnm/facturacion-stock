<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $table = 'productos';
    protected $guarded = [];

    public function marca()
    {
        return $this->belongsTo(Marca::class, 'marca_id');
    }

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id');
    }

    public function compras()
    {
        return $this->hasMany(CompraProducto::class, "producto_id");
    }


    /**
     * ACCESSORS Y MUTATORS
     */

    public function setNombreAttribute($value)
    {
        $this->attributes["nombre"] = ucfirst($value);
    }

    public function setDescripcionAttribute($value)
    {
        $this->attributes["descripcion"] = ucfirst($value);
    }
}
