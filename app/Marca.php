<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = 'marcas';
    protected $guarded = [];

    public function productos()
    {
        return $this->hasMany(Producto::class, 'marca_id');
    }

    public function setNombreAttribute($value)
    {
        $this->attributes["nombre"] = ucfirst($value);
    }
}
