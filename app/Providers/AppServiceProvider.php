<?php

namespace App\Providers;

use App\CompraProducto;
use App\Observers\CompraProductoObserver;
use App\Observers\ProductoObserver;
use App\Producto;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CompraProducto::observe(CompraProductoObserver::class);
    }
}
