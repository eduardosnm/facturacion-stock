<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    protected $table = 'compras';
    protected $withCount = ['productos'];
    protected $guarded = [];

    public function productos()
    {
        return $this->belongsToMany(Producto::class, 'compra_producto',
            'compra_id',
            'producto_id')
            ->as('compra')
            ->withPivot("cantidad", "total", "precio_unitario","producto_id", "id");
    }

    public function compraProducto()
    {
        return $this->hasMany(CompraProducto::class, 'compra_id');
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
