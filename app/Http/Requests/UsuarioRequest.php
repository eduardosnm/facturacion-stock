<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "name" => "required|string|max:255",
            "email" => "required|string|email|max:191|unique:users,email",
            "password" => "required|string|min:6",
            "is_admin" => "required|boolean"
        ];

        if (request()->method() == 'PUT'){
            $rules = [
                "name" => "required|string|max:255",
                "email" => "required|string|email|max:191|unique:users,email,".request()->id,
                "is_admin" => "required|boolean",
                "password" => "sometimes|required|min:6",
            ];
        }

        return $rules;
    }
}
