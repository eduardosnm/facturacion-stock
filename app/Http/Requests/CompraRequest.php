<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompraRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "datos.*.producto_id" => "required|numeric|exists:productos,id",
            "datos.*.cantidad" => "required|numeric|min:1",
            "datos.*.precio_unitario" => "required|numeric|min:0.1",
            "datos.*.precio_total" => "required|numeric|min:0.1",
        ];
    }

    public function messages()
    {
        return [
            'datos.*.*.required' => 'Este campo es obligatorio',
            'datos.*.producto_id.exists' => 'El producto ingresado no se corresponde con un producto existente',
            'datos.*.*.numeric' => 'Este campo debe ser numerico',
            'datos.*.*.min' => 'Este campo debe ser mayor o igual que :min',
        ];
    }
}
