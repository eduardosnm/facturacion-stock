<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "id" => "required|unique:productos,id",
            "nombre" => "required",
            "markup" => "sometimes|numeric|min:0",
            "marca_id" => "required|exists:marcas,id",
            "categoria_id" => "required|exists:categorias,id"
        ];

        if (request()->method() == 'PUT'){
            $rules["id"] = "required|unique:productos,id,".$this->producto->id;
        }

        return $rules;
    }
}
