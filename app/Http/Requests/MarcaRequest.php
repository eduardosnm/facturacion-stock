<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarcaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "nombre" => "required|unique:marcas,nombre"
        ];

        if (request()->method() == 'PUT') {
            $rules = [
                "nombre" => "required|unique:marcas,nombre,".$this->marca->id
            ];
        }

        return $rules;
    }
}
