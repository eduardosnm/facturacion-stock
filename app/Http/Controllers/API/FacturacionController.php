<?php

namespace App\Http\Controllers\API;

use App\Components\Afip;
use App\Http\Controllers\Controller;
use App\Venta;
use App\Http\Resources\Venta as VentaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FacturacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return VentaResource
     */
    public function store(Request $request)
    {
        try {
            $request->validate([
                "venta_id" => "required|exists:ventas,id"
            ]);

            $venta = Venta::find($request->venta_id);

            $afip = Afip::getInstance();

            $result = $afip->ElectronicBilling->CreateNextVoucher(
                $this->getDatos($venta)
            );

            $venta->afip()->create($result);

        }catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return response()->json(["message" => $ex->getMessage()], 500);
        }

        return new VentaResource($venta->load('productos.marca', 'productos.categoria', 'afip', 'formaDePago'));
    }

    /**
     * @param Venta $venta
     * @return array
     */
    private function getDatos(Venta $venta) : array
    {
        return [
            'CantReg' 	=> 1,  // Cantidad de comprobantes a registrar
            'PtoVta' 	=> 00001,  // Punto de venta
            /* Tipos de comprobantes
                1: Factura A
                2: Nota de Débito A
                3: Nota de Crédito A
                4: Recibo A
                6: Factura B
                7: Nota de Débito B
                8: Nota de Crédito B
                9: Recibo B
                11: Factura C
                12: Nota de Débito C
                13: Nota de Crédito C
                15: Recibo C
            */
            'CbteTipo' 	=> 11,  // Tipo de comprobante (ver tipos disponibles)
            'Concepto' 	=> 1,  // Concepto del Comprobante: (1)Productos, (2)Servicios, (3)Productos y Servicios
            'DocTipo' 	=> 99, // Tipo de documento del comprador (99 consumidor final, ver tipos disponibles)
            'DocNro' 	=> 0,  // Número de documento del comprador (0 consumidor final)
            /*
                USANDO EL METODO CREATENEXTVOUCHER NO HACE FALTA ENVIAR CbteDesde y CbteHasta
                PARA FACTURAS C DEBEN SER IGUALES
            */
//            'CbteDesde' 	=> 2,  // Número de comprobante o numero del primer comprobante en caso de ser mas de uno
//            'CbteHasta' 	=> 2,  // Número de comprobante o numero del último comprobante en caso de ser mas de uno
            'CbteFch' 	=> intval(date('Ymd')), // (Opcional) Fecha del comprobante (yyyymmdd) o fecha actual si es nulo
            'ImpTotal' 	=> $venta->total, // Importe total del comprobante
            'ImpTotConc' 	=> 0,   // Importe neto no gravado
            'ImpNeto' 	=> $venta->total, // Importe neto gravado
            'ImpOpEx' 	=> 0,   // Importe exento de IVA
            'ImpIVA' 	=> 0,  //Importe total de IVA
            'ImpTrib' 	=> 0,   //Importe total de tributos
            'MonId' 	=> 'PES', //Tipo de moneda usada en el comprobante (ver tipos disponibles)('PES' para pesos argentinos)
            'MonCotiz' 	=> 1,     // Cotización de la moneda usada (1 para pesos argentinos)
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
