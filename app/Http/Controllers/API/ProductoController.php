<?php

namespace App\Http\Controllers\API;

use App\CompraProducto;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductoRequest;
use App\Producto;
use \App\Http\Resources\Producto as ProductoResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $filled = array_filter(request()->only([
            'id',
            'nombre',
            'descripcion',
            'marca_id',
        ]));

        $productos = Producto::with('marca','categoria')
            ->with(['compras' => function($query) {
                $query->orderBy('created_at','DESC')->first();
            }])
            ->when(count($filled) > 0, function ($query) use ($filled) {
                foreach ($filled as $column => $value) {
                    $query->where($column, 'LIKE', "%{$value}%");
                }
            })->when(request('search', '') != '', function ($query) {
                $query->where(function ($q) {
                    $q->where('id', request('id'))
                        ->orWhere('nombre', 'LIKE', "%".request('search')."%")
                        ->orWhere('descripcion', 'LIKE', "%".request('search')."%");
                });
            })
            ->orderBy('nombre')
            ->paginate(20);

        return ProductoResource::collection($productos);
    }

    public function disponibles()
    {
        $filled = array_filter(request()->only([
            'id',
            'nombre',
            'descripcion',
            'marca_id',
        ]));

        $productos = Producto::with('marca','categoria')
            ->when(count($filled) > 0, function ($query) use ($filled) {
                foreach ($filled as $column => $value) {
                    $query->where($column, 'LIKE', "%{$value}%");
                }
            })->when(request('search', '') != '', function ($query) {
                $query->where(function ($q) {
                    $q->where('id', request('id'))
                        ->orWhere('nombre', 'LIKE', "%".request('search')."%")
                        ->orWhere('descripcion', 'LIKE', "%".request('search')."%");
                });
            })
            ->where('cantidad', '>', 0)
            ->where('markup', '>', 0)
            ->orderBy('nombre')
            ->paginate(20);

        return ProductoResource::collection($productos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|object
     */
    public function store(ProductoRequest $request)
    {
        $producto = Producto::create($request->all());

        return (new ProductoResource($producto))
            ->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Producto $producto
     * @return ProductoResource
     */
    public function show(Producto $producto)
    {
        $producto->load('marca');

        return new ProductoResource($producto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Producto $producto
     * @return \Illuminate\Http\JsonResponse|Response|object
     */
    public function update(ProductoRequest $request, Producto $producto)
    {
        $data = $request->except('precio_compra');

        $ultimaCompra = CompraProducto::where('producto_id', $producto->id)->latest()->first();
        $precio = ($request->has('precio_compra'))
            ? $request->precio_compra
            : $ultimaCompra->precio_unitario * (1 + $data["markup"] / 100);

        $data["precio"] = $precio;

        $producto->update($data);

        $producto->save();

        return (new ProductoResource($producto))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Producto $producto
     * @return \Illuminate\Http\JsonResponse|object|void
     * @throws \Exception
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();

        return (new ProductoResource($producto))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    public function searchProduct(Request $request)
    {
        $terms = explode(" ", $request->search);

        $productos = Producto::select("productos.id",
            DB::raw("CONCAT(productos.nombre, ' ', productos.descripcion, ' (', marcas.nombre, ')') AS label"))
            ->join("marcas", "productos.marca_id", "=", "marcas.id")
            ->where("productos.nombre", "LIKE", "%{$request->search}%")
            ->orWhere("productos.descripcion", "LIKE", "%{$request->search}%")
            ->orWhere("productos.id", $request->search)
            ->orWhere("marcas.nombre", "LIKE", "%{$request->search}%")
            ->get();

        return ProductoResource::collection($productos);
    }
}
