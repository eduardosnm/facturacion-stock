<?php

namespace App\Http\Controllers\API;

use App\Categoria;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriaRequest;
use App\Http\Resources\Categoria as CategoriaResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoriaController extends Controller
{
    public function index()
    {
        $categorias = Categoria::orderBy('nombre')
                ->paginate(20);

        return CategoriaResource::collection($categorias);
    }


    public function store(CategoriaRequest $request)
    {
        $categoria = Categoria::create($request->all());

        return (new CategoriaResource($categoria))
            ->response()->setStatusCode(Response::HTTP_CREATED);
    }

    public function update(CategoriaRequest $request, Categoria $categoria)
    {
        $categoria->update($request->all());

        return (new CategoriaResource($categoria))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    public function destroy(Categoria $categoria)
    {
        $categoria->delete();

        return (new CategoriaResource($categoria))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    public function searchCategoria(Request $request)
    {
        $categorias = Categoria::select("id", "nombre AS label")
            ->where("estado", true)
            ->where("nombre", "LIKE", "%{$request->search}%")
            ->get();

        return CategoriaResource::collection($categorias);
    }


}
