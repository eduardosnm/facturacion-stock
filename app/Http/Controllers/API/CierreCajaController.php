<?php

namespace App\Http\Controllers\API;

use App\CierreCaja;
use App\Http\Controllers\Controller;
use App\Notifications\CierreDeCajaNotification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\CierreCaja as CierreResource;

class CierreCajaController extends Controller
{
    public function index()
    {
        $cierre = CierreCaja::whereDate("created_at", date('Y-m-d'))->first();

        return new CierreResource($cierre);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $cierre = CierreCaja::create([
                "total" => $request->total,
                "user_id" => auth()->user()->id
            ]);

            DB::commit();
        }catch (\Exception $ex){
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->json(["error" => "Algo salio mal"], 500);
        }
        $correo ='fabianhbolognesi@gmail.com';
        if (App::environment('local')){
            $correo = 'eduardosnm@gmail.com';
        }
        $user = User::where("email", $correo)->first();
        $user->notify(new CierreDeCajaNotification($cierre));

        return new CierreResource($cierre);
    }

}
