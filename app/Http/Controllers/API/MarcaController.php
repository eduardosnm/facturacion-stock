<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarcaRequest;
use App\Marca;
use App\Http\Resources\Marca as MarcaResource;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $marcas = Marca::orderBy('nombre')->get();

        return MarcaResource::collection($marcas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse|Response|object
     */
    public function store(MarcaRequest $request)
    {
        $marca = Marca::create($request->all());

        return (new MarcaResource($marca))
            ->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Marca $marca
     * @return MarcaResource
     */
    public function show(Marca $marca)
    {
        return new MarcaResource($marca);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Marca $marca
     * @return \Illuminate\Http\JsonResponse|object|void
     */
    public function update(MarcaRequest $request, Marca $marca)
    {
        $marca->fill($request->all());

        if ($marca->isClean()) {
            return response()->json(["error" => "Al menos un valor debe cambiar"], Response::HTTP_CONFLICT);
        }

        $marca->save();

        return (new MarcaResource($marca))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Marca $marca
     * @return \Illuminate\Http\JsonResponse|object|void
     * @throws \Exception
     */
    public function destroy(Marca $marca)
    {
        $marca->delete();

        return (new MarcaResource($marca))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    public function searchMarca(Request $request)
    {
        $marcas = Marca::select("id", "nombre AS label")
            ->where("nombre", "LIKE", "%{$request->search}%")->get();

        return MarcaResource::collection($marcas);
    }
}
