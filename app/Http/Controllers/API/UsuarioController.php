<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsuarioRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $usuarios = User::paginate(20);

        return UserResource::collection($usuarios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource|\Illuminate\Http\JsonResponse|object
     */
    public function store(UsuarioRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->all();

            $data["password"] = bcrypt($request->password);
            $data["is_admin"] = boolval($request->is_admin) ? true : false;

            $usuario = User::create($data);

            DB::commit();
        }catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->json(["error" => "Algo salio mal"], 500);
        }

        return (new UserResource($usuario))
            ->response()->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UsuarioRequest $request
     * @param User $usuario
     * @return \Illuminate\Http\JsonResponse|Response|object
     */
    public function update(UsuarioRequest $request, User $usuario)
    {
        DB::beginTransaction();
        try {

            $usuario->update($request->all());

            DB::commit();
        }catch (\Exception $ex){
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->json(["error" => "Algo salio mal"], 500);
        }

        return (new UserResource($usuario))
            ->response()->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $usuario
     * @return \Illuminate\Http\JsonResponse|object|void
     */
    public function destroy(User $usuario)
    {
        $usuario->delete();

        return (new UserResource($usuario))
            ->response()->setStatusCode(Response::HTTP_OK);
    }
}
