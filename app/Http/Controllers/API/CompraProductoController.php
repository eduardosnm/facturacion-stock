<?php

namespace App\Http\Controllers\API;

use App\Compra;
use App\CompraProducto;
use App\Http\Resources\CompraProducto as CompraProductoResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompraProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Compra $compra
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Compra $compra)
    {
        $items = $compra->productos;

        return CompraProductoResource::collection($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Compra $compra
     * @param CompraProducto $compraProducto
     * @return CompraProductoResource
     */
    public function destroy($compra, $compraProducto)
    {
        $compraProducto = CompraProducto::find($compraProducto);
        $compraProducto->delete();

        return new CompraProductoResource($compraProducto);
    }
}
