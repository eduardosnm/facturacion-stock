<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Venta;
use Barryvdh\DomPDF\Facade;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function show($id)
    {
        $venta = Venta::with('productos.marca', 'productos.categoria', 'afip', 'formaDePago')
            ->findOrFail($id);

        $pdf = $this->generarPdf($venta);

        return $pdf->output();
    }

    private function generarPdf($venta)
    {
        return Facade::loadView('pdf.factura', compact('venta'));
    }

}
