<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\ProductoVenta;
use App\Venta;
use App\Http\Resources\Venta as VentaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $ventas = Venta::withCount('productos')
            ->with('usuario', 'formaDePago')
            ->orderBy('id', 'DESC')
            ->paginate(20);

        return VentaResource::collection($ventas);
    }

    public function delDia()
    {
        $ventas = Venta::withCount('productos')
            ->with('usuario', 'formaDePago')
            ->whereDate("created_at", date('Y-m-d'))
            ->orderBy('id', 'DESC')
            ->get();

        return VentaResource::collection($ventas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return VentaResource
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $total = 0;
            $venta = Venta::create([
                "user_id" => auth()->user()->id,
                "forma_pago_id" => $request->forma_pago_id
            ]);

            $productos = $request->carrito;

            foreach ($productos as $dato){
                $productoVenta = ProductoVenta::create([
                    "producto_id" => $dato["producto"]["id"],
                    "cantidad" => $dato["cantidad"],
                    "precio_unitario" => $dato["producto"]["precio"],
                    "total" => $dato["cantidad"] * $dato["producto"]["precio"],
                    "venta_id" => $venta->id
                ]);

                $total += $productoVenta->total;
                $producto = $productoVenta->producto;
                $producto->update([
                    "cantidad" => $producto->cantidad - $dato["cantidad"],
                ]);
            }

            $venta->update(["total" => $total]);
            DB::commit();
        }catch (\Exception $ex){
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->json(["error" => "Algo salio mal"], 500);
        }

        return new VentaResource($venta);
    }

    /**
     * Display the specified resource.
     *
     * @param Venta $venta
     * @return VentaResource
     */
    public function show(Venta $venta)
    {
        $venta->load('productos.marca', 'productos.categoria', 'afip', 'formaDePago');

        return new VentaResource($venta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
