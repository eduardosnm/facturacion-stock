<?php

namespace App\Http\Controllers\API;

use App\Compra;
use App\CompraProducto;
use App\Http\Requests\CompraRequest;
use App\Http\Resources\Compra as CompraResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $compras = Compra::withCount('productos')
            ->with('usuario')
            ->orderBy('id', 'DESC')
            ->paginate(10);

        return CompraResource::collection($compras);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CompraResource
     */
    public function store(CompraRequest $request)
    {
        DB::beginTransaction();
        try {
            $total = 0;
            $compra = Compra::create(["user_id" => auth()->user()->id]);
            $datos = $request->datos;
            foreach ($datos as $dato){
                $compraProducto = CompraProducto::create([
                    "producto_id" => $dato["producto_id"],
                    "cantidad" => $dato["cantidad"],
                    "precio_unitario" => $dato["precio_unitario"],
                    "total" => $dato["precio_total"],
                    "compra_id" => $compra->id
                ]);

                $total += $dato["precio_total"];
                $producto = $compraProducto->producto;
                $producto->update([
                    "precio" => $producto->markup != 0 ? $compraProducto->precio_unitario * (1 + $producto->markup / 100) : 0,
                    "cantidad" => $producto->cantidad + $dato["cantidad"],
                ]);
            }

            $compra->update(["total" => $total]);

            DB::commit();
        }catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex->getMessage());
            return response()->json(["error" => "Algo salio mal"], 500);
        }
        return new CompraResource($compra);
    }

    /**
     * Display the specified resource.
     *
     * @param Compra $compra
     * @return CompraResource
     */
    public function show(Compra $compra)
    {
        $compra->withCount('productos');

        return new CompraResource($compra);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Compra $compra
     * @return CompraResource
     */
    public function destroy(Compra $compra)
    {
        $compra->delete();

        return new CompraResource($compra);
    }
}
