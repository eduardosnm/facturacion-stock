<?php


namespace App\Components;


class Afip
{

    public static function getInstance()
    {
        return new \Afip([
            "CUIT"          =>  config('afip.cuit'),
            "production"    =>  config('afip.production'),
            "cert"          =>  config('afip.cert'),
            "key"           =>  config('afip.key'),
            "passphrase"    =>  config('afip.passphrase'),
            "res_folder"    =>  config('afip.res_folder')
        ]);
    }
}
