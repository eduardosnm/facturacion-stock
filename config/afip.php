<?php

return [
    "cuit"          => env('AFIP_CUIT', 23305072699),
    "production"    => env('AFIP_PRODUCTION', false),
    "cert"          => "cert",
    "key"           => "key",
    "passphrase"    => env('AFIP_PASSPHRASE', 'xxxxx'),
    "res_folder"    => storage_path('afip-cert/'),
];
