<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')
    ->namespace('API\\')
    ->group(function (){
        Route::get("productos/buscar", "ProductoController@searchProduct");
        Route::get("productos/disponibles", "ProductoController@disponibles");
        Route::apiResource("productos","ProductoController");
        Route::get("marcas/buscar", "MarcaController@searchMarca");
        Route::apiResource("marcas","MarcaController");
        Route::apiResource("productos","ProductoController");
        Route::apiResource("compras","CompraController");
        Route::apiResource("compras/{compra}/productos","CompraProductoController")
            ->except(['show', 'store', 'update']);
        Route::get("ventas/hoy","VentaController@delDia");
        Route::apiResource("ventas","VentaController");
        Route::get("categorias/buscar", "CategoriaController@searchCategoria");
        Route::apiResource("categorias","CategoriaController");
        Route::apiResource("pdf","PdfController")->only(['show']);
        Route::apiResource("usuarios","UsuarioController");
        Route::apiResource('cierre', 'CierreCajaController')
            ->only(['index', 'store']);
        Route::apiResource('facturacion', 'FacturacionController')
            ->only(['store']);
    });





